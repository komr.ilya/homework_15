//
//  main.cpp
//  homework_15
//
//  Created by Ilya Komrakov on 26.04.2020.
//  Copyright © 2020 Ilya Komrakov. All rights reserved.
//

#include <iostream>

const int N = 12;

// Print to console odd or even values
int PrintNum(bool a, int n)
{
    int i = 0;
    
    if (!a) {
        i = 1;
    }
    
    for (i; i < n; i += 2) {
        std::cout << i << std::endl;
    }
    
    return 0;
}

int main()
{
    // print to console all even values
    for (int i = 0; i < N; i++)
    {
        if (i % 2 == 0)
        {
            std::cout << i << std::endl;
        }

    }
    
    std::cout << "\n";
    
    
    PrintNum(1, N);
    std::cout << "\n";
    
    PrintNum(0, N);
    
    
    return 0;
}
